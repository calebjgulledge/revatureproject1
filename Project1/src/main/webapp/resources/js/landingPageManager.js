




window.onload = function () {
    startupOrderStr();
    startUpNamePlate();
    startUpTickets();
    document.getElementById("refreshBtn").addEventListener('click', restartUpTickets)
    document.getElementById("editReimbBtn").addEventListener('click', editTicket)
    document.getElementById("pendAsc").addEventListener('click', setOrderStr)
    document.getElementById("pendDesc").addEventListener('click', setOrderStr)
    document.getElementById("allAsc").addEventListener('click', setOrderStr)
    document.getElementById("allDesc").addEventListener('click', setOrderStr)


}

let classUserVar = -1;

function startUpNamePlate() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let user = JSON.parse(xhttp.responseText);
            classUserVar = user;
            let welcomeName = document.querySelector("#welcomeName");
            let welcome = "Hello " + user.firstName + "!";
            welcomeName.innerText = welcome;
        }
    }
    xhttp.open('GET', "http://localhost:9001/Project1/user/get", false)

    xhttp.send();
}

function startUpTickets() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            createTicketTable(reimbArray);

        }
    }
    xhttp.open('GET', "http://localhost:9001/Project1/action/find_pending", true)


    xhttp.send();

}

function restartUpTickets() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            updateTicketTable(reimbArray);

        }
    }
    xhttp.open('GET', "http://localhost:9001/Project1/action/find_pending", true)


    xhttp.send();
}

function createTicketTable(respObj) {

    let cardPerRow = 4;
    let rowName = "bigRow0";
    let ticketHolder = document.querySelector('#ticketHolder');
    let colLength = 12 / cardPerRow;

    for (let i = 0; i < respObj.length; i++) {
        if (i % cardPerRow == 0) {
            let rowID = i / cardPerRow;

            ticketHolder.appendChild(document.createElement("br"));

            let newRow = document.createElement('div');
            newRow.setAttribute('class', "row justify-content-center");
            rowName = "bigRow" + rowID;
            newRow.setAttribute('id', rowName);
            ticketHolder.appendChild(newRow);
        }
        let newRow = document.querySelector("#" + rowName);
        createTicketCard(respObj, i, newRow, colLength);
    }


}

function createTicketCard(respObj, i, newRow, colLength) {
    let columnLabel = document.createElement("div");
    columnLabel.setAttribute("class", "col-" + colLength);
    columnLabel.setAttribute("id", "col" + i)
    newRow.appendChild(columnLabel);

    let cardContainer = document.createElement("div");
    cardContainer.setAttribute("class", "container");
    columnLabel.appendChild(cardContainer);

    let cardLabel = document.createElement("div");
    cardLabel.setAttribute("class", "card");
    cardLabel.setAttribute("style", "width: 16rem;");
    cardLabel.setAttribute("id", "ticketCard" + i);
    cardContainer.appendChild(cardLabel);


    let cardBody = document.createElement("div");
    cardBody.setAttribute("class", "card-body");
    cardBody.setAttribute("id", "cardBody" + i)
    cardLabel.appendChild(cardBody);

    //Fun Starts
    let cardTitle = document.createElement("h5");
    cardTitle.setAttribute("class", "card-title")
    cardTitle.innerText = "Ticket ID #" + respObj[i].reimbID;
    cardBody.appendChild(cardTitle);

    let cardOwner = document.createElement("h6");
    cardOwner.setAttribute("id", "eName" + i)
    cardOwner.innerText = "Employee ID: " + respObj[i].reimbAuthorID;
    updateIDToName(respObj[i].reimbAuthorID, cardOwner, 0)
    cardBody.appendChild(cardOwner);
    
    let cardStatus = document.createElement("h6");
    cardStatus.innerText = "Status: " + reimbStatusReverseCase(respObj[i].reimbStatus);
    cardBody.appendChild(cardStatus);


    let cardAmount = document.createElement("h6");
    cardAmount.innerText = "Amount: $" + respObj[i].reimbAmount;
    cardBody.appendChild(cardAmount);


    let cardDescription = document.createElement("h6");
    cardDescription.innerText = "Description: " + respObj[i].reimbDescription;
    cardBody.appendChild(cardDescription);


    let cardType = document.createElement("h6");
    cardType.innerText = "Type: " + reimbTypeReverseCase(respObj[i].reimbType);
    cardBody.appendChild(cardType);


    let cardSubmitDate = document.createElement("h6");
    let sEvent = new Date(respObj[i].reimbSubmittedDate).toLocaleDateString();
    cardSubmitDate.innerText = "Submitted: " + sEvent;
    cardBody.appendChild(cardSubmitDate);

    if (respObj[i].reimbResolvedDate != null) {
        let cardResolvedDate = document.createElement("h6");
        let rEvent = new Date(respObj[i].reimbResolvedDate).toLocaleDateString();
        cardResolvedDate.innerText = "Resolved: " + rEvent;
        cardBody.appendChild(cardResolvedDate);

        let cardResolverID = document.createElement("h6");
        updateIDToName(respObj[i].reimbResolverID, cardResolverID, 1);
        cardResolverID.innerText = "Resolver ID: " + respObj[i].reimbResolverID;
        cardBody.appendChild(cardResolverID);
    }

    // let cardBtn = document.createElement("input");
    // cardBtn.setAttribute("type", "button");
    // cardBtn.setAttribute("id", i);
    // cardBtn.setAttribute("value", "Edit");
    // cardBtn.setAttribute("class", "btn btn-primary");
    // cardBody.appendChild(cardBtn);

    // document.getElementById(i).addEventListener('click', editTicket)

}



function updateTicketTable(respObj) {
    let container = document.querySelector("#mainContainer")
    let ticketHolder = document.querySelector("#ticketHolder")
    ticketHolder.remove();
    let newTicketHolder = document.createElement('div');
    newTicketHolder.setAttribute('id', 'ticketHolder');
    container.appendChild(newTicketHolder);
    createTicketTable(respObj);
}


function editTicket() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            restartUpTickets();
        }
    }
    xhttp.open('POST', "http://localhost:9001/Project1/action/update_status", false)

    xhttp.setRequestHeader("Content-Type", "application/json");
    let newTicket = {
        "reimbID": document.querySelector("#ticketID").value,
        "reimbDescription": document.querySelector("#newDescr").value,
        "reimbStatus": document.querySelector("#reimbStatus").value
    }
    console.log(newTicket);

    xhttp.send(JSON.stringify(newTicket));
}


function setOrderStr(clickRef) {
    let tarID = clickRef.target.id;
    console.log(tarID);
    let orderString = "";
    let orderType = "";
    switch (tarID) {
        case "allAsc":
            orderString = "ORDER BY reimb_id asc";
            orderType = "all";
            sendStrOrder(orderString, orderType);
            break;
        case "allDesc":
            orderString = "ORDER BY reimb_id desc";
            orderType = "all";
            sendStrOrder(orderString, orderType);
            break;
        case "pendAsc":
            orderString = "ORDER BY reimb_id asc"
            orderType = "pend";
            sendStrOrder(orderString, orderType);
            break;
        case "pendDesc":
            orderString = "ORDER BY reimb_id desc"
            orderType = "pend";
            sendStrOrder(orderString, orderType);
            break;
    }

    

}
function sendStrOrder(orderString, orderType) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            console.log("What does the JSON say?")
            let reimbArray = JSON.parse(xhttp.responseText);
            updateTicketTable(reimbArray);
        }
    }

    let params = "?orderString=" + orderString + "&orderType=" + orderType;

    xhttp.open('GET', "http://localhost:9001/Project1/action/setorder" + params, false)

    xhttp.send();
}


function startupOrderStr() {
    let orderString = "";
    let orderType = "first";
    
    sendFirstStrOrder(orderString, orderType);
}

function sendFirstStrOrder(orderString, orderType) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);
        if (xhttp.readyState == 4 && xhttp.status == 200) {
        }
    }

    let params = "?orderString=" + orderString + "&orderType=" + orderType;

    xhttp.open('GET', "http://localhost:9001/Project1/action/setorder" + params)

    xhttp.send();
}


function updateIDToName (ID, div, role) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let uModel = JSON.parse(xhttp.responseText);
            if(role == 0) {
                div.innerText = "Employee: " + uModel.firstName + " " + uModel.lastName;
            } else {
                div.innerText = "Resolver: " + uModel.firstName + " " + uModel.lastName
            }
            
        }
    }

    let params = "?userID=" + ID;

    xhttp.open('GET', "http://localhost:9001/Project1/user/get_id" + params)

    xhttp.send();
}


function reimbTypeSwitchCase(reimbTypeString) {
    switch (reimbTypeString.toLowerCase()) {
        case "0":
        case "other":
            return 0;
        case "1":
        case "food":
            return 1;
        case "2":
        case "lodging":
            return 2;
        case "3":
        case "travel":
            return 3;
        default:
            return 0;
    }

}

function reimbTypeReverseCase(reimbTypeInt) {
    switch (reimbTypeInt) {
        case 0:
        case "other":
            return "OTHER";
        case 1:
        case "food":
            return "FOOD";
        case 2:
        case "lodging":
            return "LODGING";
        case 3:
        case "travel":
            return "TRAVEL";
        default:
            return "OTHER";
    }
}

function reimbStatusReverseCase(reimbTypeInt) {
    switch (reimbTypeInt) {
        case 0:
        case "denied":
            return "DENIED";
        case 1:
        case "approved":
            return "APPROVED";
        case 2:
        case "pending":
            return "PENDING";;
    }
}