




window.onload = function () {
    startUpNamePlate();
    startupOrderStr();
    document.getElementById("createReimbBtn").addEventListener('click', createTicket)
    document.getElementById("refreshBtn").addEventListener('click', restartUpTickets)
    document.getElementById("IDAscending").addEventListener('click', setOrderStr)
    document.getElementById("IDDescending").addEventListener('click', setOrderStr)
    document.getElementById("statusAscending").addEventListener('click', setOrderStr)
    document.getElementById("statusDescending").addEventListener('click', setOrderStr)
}

let classUserVar = -1;

function startUpNamePlate() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let user = JSON.parse(xhttp.responseText);
            classUserVar = user;
            let welcomeName = document.querySelector("#welcomeName");
            let welcome = "Hello " + user.firstName + "!";
            welcomeName.innerText = welcome;
            startUpTickets();
        }
    }
    xhttp.open('GET', "http://localhost:9001/Project1/user/get", false)

    xhttp.send();
}

function startUpTickets() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            createTicketTable(reimbArray);

        }
    }
    xhttp.open('GET', "http://localhost:9001/Project1/action/findall", false)


    xhttp.send();
}

function restartUpTickets() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            updateTicketTable(reimbArray);

        }
    }
    xhttp.open('GET', "http://localhost:9001/Project1/action/findall", false)


    xhttp.send();
}



function createTicketTable(respObj) {

    let cardPerRow = 4;
    let rowName = "bigRow0";
    let ticketHolder = document.querySelector('#ticketHolder');
    let colLength = 12 / cardPerRow;

    for (let i = 0; i < respObj.length; i++) {
        if (i % cardPerRow == 0) {
            let rowID = i / cardPerRow;
            
            ticketHolder.appendChild(document.createElement("br"));

            let newRow = document.createElement('div');
            newRow.setAttribute('class', "row justify-content-center");
            rowName = "bigRow" + rowID;
            newRow.setAttribute('id', rowName);
            ticketHolder.appendChild(newRow);
        }
        let newRow = document.querySelector("#" + rowName);
        createTicketCard(respObj, i, newRow, colLength);
    }


}

function createTicketCard(respObj, i, newRow, colLength) {
    let columnLabel = document.createElement("div");
    columnLabel.setAttribute("class", "col-" + colLength);
    columnLabel.setAttribute("id", "col" + i)
    newRow.appendChild(columnLabel);

    let cardContainer = document.createElement("div");
    cardContainer.setAttribute("class", "container");
    columnLabel.appendChild(cardContainer);

    let cardLabel = document.createElement("div");
    cardLabel.setAttribute("class", "card");
    cardLabel.setAttribute("style", "width: 16rem;");
    cardLabel.setAttribute("id", "ticketCard" + i);
    cardContainer.appendChild(cardLabel);


    let cardBody = document.createElement("div");
    cardBody.setAttribute("class", "card-body");
    cardBody.setAttribute("id", "cardBody" + i)
    cardLabel.appendChild(cardBody);

    //Fun Starts
    let cardTitle = document.createElement("h5");
    cardTitle.setAttribute("class", "card-title")
    cardTitle.innerText = "Ticket ID #" + respObj[i].reimbID;
    cardBody.appendChild(cardTitle);

    let cardStatus = document.createElement("h6");
    cardStatus.innerText = "Status: " + reimbStatusReverseCase(respObj[i].reimbStatus);
    cardBody.appendChild(cardStatus);


    let cardAmount = document.createElement("h6");
    cardAmount.innerText = "Amount: $" + respObj[i].reimbAmount;
    cardBody.appendChild(cardAmount);


    let cardDescription = document.createElement("h6");
    cardDescription.innerText = "Description: " + respObj[i].reimbDescription;
    cardBody.appendChild(cardDescription);


    let cardType = document.createElement("h6");
    cardType.innerText = "Type: " + reimbTypeReverseCase(respObj[i].reimbType);
    cardBody.appendChild(cardType);


    let cardSubmitDate = document.createElement("h6");
    let sEvent = new Date(respObj[i].reimbSubmittedDate).toLocaleDateString();
    cardSubmitDate.innerText = "Submitted: " + sEvent;
    cardBody.appendChild(cardSubmitDate);

    if (respObj[i].reimbResolvedDate != null) {
        let cardResolvedDate = document.createElement("h6");
        let rEvent = new Date(respObj[i].reimbResolvedDate).toLocaleDateString();
        cardResolvedDate.innerText = "Resolved: " + rEvent;
        cardBody.appendChild(cardResolvedDate);

        let cardResolverID = document.createElement("h6");
        cardResolverID.innerText = "Resolver ID: " + respObj[i].reimbResolverID;
        updateIDToName(respObj[i].reimbResolverID, cardResolverID);
        cardBody.appendChild(cardResolverID);
    }

    // let cardBtn = document.createElement("input");
    // cardBtn.setAttribute("type", "button");
    // cardBtn.setAttribute("id", i);
    // cardBtn.setAttribute("value", "Edit");
    // cardBtn.setAttribute("class", "btn btn-primary");
    // cardBody.appendChild(cardBtn);

    // document.getElementById(i).addEventListener('click', editTicket)

}



function updateTicketTable(respObj) {
    let container = document.querySelector("#mainContainer")
    let ticketHolder = document.querySelector("#ticketHolder")
    ticketHolder.remove();
    let newTicketHolder = document.createElement('div');
    newTicketHolder.setAttribute('id', 'ticketHolder');
    container.appendChild(newTicketHolder);
    createTicketTable(respObj);
}


function createTicket() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbTicket = JSON.parse(xhttp.responseText);
            console.log(reimbTicket);
            restartUpTickets();
        }
    }
    xhttp.open('POST', "http://localhost:9001/Project1/action/create", false)

    xhttp.setRequestHeader("Content-Type", "application/json");
    let newTypeInt = reimbTypeSwitchCase(document.querySelector("#reimbType").value);
    let newTicket = {
        "reimbAmount": document.querySelector("#newAmount").value,
        "reimbDescription": document.querySelector("#newDescr").value,
        "reimbType": newTypeInt
    }
    console.log(newTicket);

    xhttp.send(JSON.stringify(newTicket));
}


function editTicket(ticketRef) {
    let btnID = ticketRef.target.id;
    let i = parseInt(btnID);
    console.log(i);

    let cardFam = document.getElementById("cardBody" + i).children
    console.log(cardFam)


    updateTicket(i);
}

function updateTicket(i) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            updateTicketTable(reimbArray);

        }
    }
    xhttp.open('GET', "http://localhost:9001/Project1/action/findall", false)


    xhttp.send();
}

function updateIDToName(ID, div) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let uModel = JSON.parse(xhttp.responseText);
            div.innerText = "Resolver: " + uModel.firstName + " " + uModel.lastName


        }
    }

    let params = "?userID=" + ID;

    xhttp.open('GET', "http://localhost:9001/Project1/user/get_id" + params)

    xhttp.send();
}

function setOrderStr(clickRef) {
    let tarID = clickRef.target.id;
    console.log(tarID);
    let orderString = "";
    let orderType = "";
    switch (tarID) {
        case "IDAscending":
            orderString = "ORDER BY reimb_id asc"
            break;
        case "IDDescending":
            orderString = "ORDER BY reimb_id desc"
            break;
        case "statusAscending":
            orderString = "ORDER BY reimb_status_fk asc"
            break;
        case "statusDescending":
            orderString = "ORDER BY reimb_status_fk desc"
            break;
    }

    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            updateTicketTable(reimbArray);
        }
    }

    let params = "?orderString=" + orderString + "&orderType=" + orderType;

    xhttp.open('GET', "http://localhost:9001/Project1/action/setorder" + params)

    xhttp.send();

}

function startupOrderStr() {
    let orderString = "";
    let orderType = "";

    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log(xhttp.readyState);
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            updateTicketTable(reimbArray);
        }
    }

    let params = "?orderString=" + orderString + "&orderType=" + orderType;

    xhttp.open('GET', "http://localhost:9001/Project1/action/setorder" + params)

    xhttp.send();
}


function reimbTypeSwitchCase(reimbTypeString) {
    switch (reimbTypeString.toLowerCase()) {
        case "0":
        case "other":
            return 0;
        case "1":
        case "food":
            return 1;
        case "2":
        case "lodging":
            return 2;
        case "3":
        case "travel":
            return 3;
        default:
            return 0;
    }

}

function reimbTypeReverseCase(reimbTypeInt) {
    switch (reimbTypeInt) {
        case 0:
        case "other":
            return "OTHER";
        case 1:
        case "food":
            return "FOOD";
        case 2:
        case "lodging":
            return "LODGING";
        case 3:
        case "travel":
            return "TRAVEL";
        default:
            return "OTHER";
    }
}

function reimbStatusReverseCase(reimbTypeInt) {
    switch (reimbTypeInt) {
        case 0:
        case "denied":
            return "DENIED";
        case 1:
        case "approved":
            return "APPROVED";
        case 2:
        case "pending":
            return "PENDING";;
    }
}


