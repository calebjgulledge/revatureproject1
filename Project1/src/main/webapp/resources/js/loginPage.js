

window.onload = function () {
    document.getElementById("loginButton").addEventListener('click', loginFunc);
 }
 
 function loginFunc() {
     let xhttp = new XMLHttpRequest();
 
     xhttp.onreadystatechange = function () {
         console.log(xhttp.readyState);
         if (xhttp.readyState == 4 && xhttp.status == 200) {
             if(xhttp != null && xhttp != undefined) {
                window.location = xhttp.responseText;
             } else {
                 let credTag = document.querySelector("#credentialTag");
                 credTag.innerText = "Credential Error, please try again";
             }
         }
     }
     
     let nameStr = document.getElementById('username').value;
     let passStr = document.getElementById("password").value;
     let params = 'username=' + nameStr + "&password=" + passStr;
     xhttp.open('POST', "http://localhost:9001/Project1/login", true);
 
     xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
 
     xhttp.send(params);

 }