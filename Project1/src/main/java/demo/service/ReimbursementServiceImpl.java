package demo.service;

import java.sql.Date;
import java.util.ArrayList;

import demo.dao.ReimbursementDAOImpl;
import demo.model.ReimbursementModel;
import demo.model.UserModel;
import lombok.NoArgsConstructor;

public class ReimbursementServiceImpl implements ReimbursementService {

	ReimbursementDAOImpl rDao = null;
	
	public ReimbursementServiceImpl() {
		super();
		this.rDao = new ReimbursementDAOImpl();
	}
	
	public ReimbursementServiceImpl(ReimbursementDAOImpl rDao) {
		super();
		this.rDao = rDao;
	}

	public ReimbursementDAOImpl getrDao() {
		return rDao;
	}

	public void setrDao(ReimbursementDAOImpl rDao) {
		this.rDao = rDao;
	}
	
	
	
	

	@Override
	public int createReimbTicket(UserModel uModel, ReimbursementModel rModel) {

		return rDao.insertReimbTicket(uModel, rModel);
	}

	@Override
	public ArrayList<ReimbursementModel> findAllReimbTickets(UserModel uModel, String orderString) {

		return rDao.selectAllReimbTickets(uModel, orderString);
	}
	
	public ArrayList<ReimbursementModel> findAllTickets(UserModel uModel, String orderString) {
		
		return rDao.selectAllTickets(uModel, orderString);
	}

	@Override
	public ReimbursementModel findReimbTicketByID(int reimbID, UserModel uModel) {

		return rDao.selectReimbTicketByID(reimbID, uModel);
	}

	@Override
	public ArrayList<ReimbursementModel> findAllPendingTickets(String orderString) {

		return rDao.selectAllPendingTickets(orderString);
	}

	@Override
	public ArrayList<ReimbursementModel> findAllPendingTicketsByEmployee(String eName) {

		return rDao.selectAllPendingTicketsByEmployee(eName);
	}

	@Override
	public ArrayList<ReimbursementModel> findAllPendingTicketsBySubmissionDate(Date date) {

		return rDao.selectAllPendingTicketsBySubmissionDate(date);
	}

	@Override
	public boolean updateReimbTicketAmount(int reimbID, int newAmount) {

		return rDao.updateReimbTicketAmount(reimbID, newAmount);
	}

	@Override
	public boolean updateReimbStatus(UserModel uModel, int reimbID, int newStatus) {

		return rDao.updateReimbStatus(uModel, reimbID, newStatus);
	}

	@Override
	public boolean updateReimbTicketDescription(int reimbID, String newDescr) {

		return rDao.updateReimbTicketDescription(reimbID, newDescr);
	}

	@Override
	public boolean deleteReimbTicket(int reimbID) {

		return rDao.removeReimbTicket(reimbID);
	}

	

}
