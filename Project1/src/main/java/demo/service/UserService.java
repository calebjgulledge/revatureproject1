package demo.service;

import demo.model.UserModel;

public interface UserService {
	
	
//	Read
	public UserModel loginAuthentication(String username, String password);
	public UserModel findUserbyName(String username);
	public UserModel findUserbyID(int uModelID);
	
//	Update
	public UserModel updateUsername(UserModel uModel, String newName);
	public UserModel updateEmail(UserModel uModel, String newEmail);
	public UserModel updatePassword(UserModel uModel, String newPassword);
	

}
