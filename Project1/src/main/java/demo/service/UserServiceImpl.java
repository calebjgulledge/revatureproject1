package demo.service;

import demo.dao.UserDAOImpl;
import demo.model.UserModel;

public class UserServiceImpl implements UserService {
	UserDAOImpl userD = new UserDAOImpl();
	@Override
	public UserModel loginAuthentication(String username, String password) {

		return userD.loginAuthentication(username, password);
	}

	@Override
	public UserModel findUserbyName(String username) {

		return userD.selectUserbyName(username);
	}

	@Override
	public UserModel findUserbyID(int uModelID) {

		return userD.selectUserbyID(uModelID);
	}

	@Override
	public UserModel updateUsername(UserModel uModel, String newName) {

		return userD.updateUsername(uModel, newName);
	}

	@Override
	public UserModel updateEmail(UserModel uModel, String newEmail) {

		return userD.updateEmail(uModel, newEmail);
	}

	@Override
	public UserModel updatePassword(UserModel uModel, String newPassword) {

		return userD.updatePassword(uModel, newPassword);
	}

}
