package demo.service;

import java.sql.Date;
import java.util.ArrayList;

import demo.model.ReimbursementModel;
import demo.model.UserModel;

public interface ReimbursementService {

//	Create
	//Employee
	//FinanceManager
	public int createReimbTicket(UserModel uModel, ReimbursementModel rModel);
	
//	Read
	//Employee
	public ArrayList<ReimbursementModel> findAllReimbTickets(UserModel uModel, String orderString);
	public ReimbursementModel findReimbTicketByID(int reimbID, UserModel uModel);
	
	
	//FinanceManager
	public ArrayList<ReimbursementModel> findAllPendingTickets(String orderString); //Not owned tickets
	public ArrayList<ReimbursementModel> findAllTickets(UserModel uModel, String orderString);
	public ArrayList<ReimbursementModel> findAllPendingTicketsByEmployee(String eName);
	public ArrayList<ReimbursementModel> findAllPendingTicketsBySubmissionDate(Date date);
	
//	Update
	//Employee
	public boolean updateReimbTicketAmount(int reimbID, int newAmount);
	
	//FinanceManager
	public boolean updateReimbStatus(UserModel uModel, int reimbID, int newStatus); //Cannot update own tickets, also needs a service layer that ensures it is a valid int in the DB
	public boolean updateReimbTicketDescription(int reimbID, String newDescr);
	
//	Delete
	//Employee
	//FinanceManager
	public boolean deleteReimbTicket(int reimbID);
	
}
