package demo.model;

import java.sql.Timestamp;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReimbursementModel {

	private int reimbID;
	private int reimbAmount; // req
	private Timestamp reimbSubmittedDate;
	private Timestamp reimbResolvedDate;
	private String reimbDescription; // req
	private int reimbAuthorID; // req
	private int reimbResolverID;
	private int reimbStatus;
	private int reimbType; // req

	public ReimbursementModel(int reimbAmount, String reimbDescription, int reimbType) {
		this.reimbAmount = reimbAmount;
		this.reimbDescription = reimbDescription;
		this.reimbType = reimbType;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReimbursementModel other = (ReimbursementModel) obj;
		return reimbAmount == other.reimbAmount && reimbAuthorID == other.reimbAuthorID
				&& Objects.equals(reimbDescription, other.reimbDescription) && reimbID == other.reimbID
				&& reimbResolverID == other.reimbResolverID && reimbStatus == other.reimbStatus
				&& reimbType == other.reimbType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(reimbAmount, reimbAuthorID, reimbDescription, reimbID, reimbResolverID, reimbStatus,
				reimbType);
	}

}
