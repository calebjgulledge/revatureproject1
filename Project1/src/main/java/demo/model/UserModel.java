package demo.model;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {
	
	private int userID;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String userEmail;
	private int userRole;
	
	
	public UserModel(int userID) {
		super();
		this.userID = userID;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserModel other = (UserModel) obj;
		return Objects.equals(firstName, other.firstName) && Objects.equals(lastName, other.lastName)
				&& Objects.equals(password, other.password) && Objects.equals(userEmail, other.userEmail)
				&& userID == other.userID && userRole == other.userRole && Objects.equals(username, other.username);
	}


	@Override
	public int hashCode() {
		return Objects.hash(firstName, lastName, password, userEmail, userID, userRole, username);
	}

	
}
