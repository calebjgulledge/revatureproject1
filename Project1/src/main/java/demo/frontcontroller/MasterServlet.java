package demo.frontcontroller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name= "MasterServlet", urlPatterns= { "/login", "/action/*", "/user/*", "/logout/*" })
public class MasterServlet extends HttpServlet {

//	Authentication Logic
	protected boolean authCheck(HttpServletRequest req) {
		if (req.getRequestURI().equals("/Project1/login") || req.getSession(false) != null)
			return true;
		else
			return false;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("In the doGet Method");
		if (authCheck(req))
			Dispatcher.virtualRouterMethod(req, resp);
		else
			req.getRequestDispatcher("resources/html/notLoggedIn.html").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("In the doPost Method");
		if (authCheck(req))
			Dispatcher.virtualRouterMethod(req, resp);
		else
			req.getRequestDispatcher("resources/html/notLoggedIn.html").forward(req, resp);
	}

}