package demo.frontcontroller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import demo.controllers.LoginController;
import demo.controllers.ReimbursementController;
import demo.controllers.UserController;

public class Dispatcher {

	public static void virtualRouterMethod(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ReimbursementController rCon = new ReimbursementController();
		LoginController lCon = new LoginController();
		UserController uCon = new UserController();
		System.out.println(req.getRequestURI());
		
		switch (req.getRequestURI()) {

/////////////////////////////////TICKETS////////////////////////////
		
		case "/Project1/action/create":
			System.out.println("case 1");
			rCon.createReimbTicket(req, resp);
			break;

		case "/Project1/action/findall":
			System.out.println("case 2.0");
			rCon.findAllReimbTickets(req, resp);
			break;

		case "/Project1/action/find":
			System.out.println("case 2.1");
			rCon.findReimbTicketByID(req, resp);
			break;
			
		case "/Project1/action/find_pending":
			System.out.println("case 2.2");
			rCon.findAllPendingTickets(req, resp);
			break;
			
		case "/Project1/action/find_pending_employee":
			System.out.println("case 2.3");
			rCon.findAllPendingTicketsByEmployee(req, resp);
			break;
			
		case "/Project1/action/find_pending_date":
			System.out.println("case 2.4");
			rCon.findAllPendingTicketsBySubmissionDate(req, resp);
			break;
			
		case "/Project1/action/all":
			System.out.println("case 2.5");
			rCon.findAllTickets(req, resp);
			break;

		case "/Project1/action/update_amount":
			System.out.println("case 3.0");
			rCon.updateReimbTicketAmount(req, resp);
			break;
			
		case "/Project1/action/update_status":
			System.out.println("case 3.1");
			rCon.updateReimbStatus(req, resp);
			break;

		case "/Project1/action/delete":
			System.out.println("case 4");
			rCon.deleteReimbTicket(req, resp);
			break;
			
		case "/Project1/action/setorder":
			System.out.println("setting order");
			rCon.setOrderString(req, resp);
			break;
			
			
////////////////////////////////USER/////////////////////////
			
		case "Project1/user":
			System.out.println("case account options");
			uCon.forwardAccountOptions(req, resp);
			break;
		case "Project1/user/backhome":
			System.out.println("back from account options");
			uCon.forwardLandingPage(req, resp);
			
		case "/Project1/user/update_email":
			System.out.println("case 5.0");
			uCon.updateEmail(req, resp);
			break;
			
		case "/Project1/user/update_username":
			System.out.println("case 5.1");
			uCon.updateUsername(req, resp);
			break;
			
		case "/Project1/user/update_password":
			System.out.println("case 5.2");
			uCon.updatePassword(req, resp);
			break;
			
		case "/Project1/login":
			System.out.println("case 6");
			lCon.login(req, resp);
			break;
			
		case "/Project1/logout":
			System.out.println("case 7");
			lCon.logout(req, resp);
			break;
			
		case "/Project1/user/get":
			System.out.println("geting user");
			lCon.getUserAccount(req, resp);
			break;
			
		case "/Project1/user/get_id":
			System.out.println("geting userID");
			uCon.findUserByID(req, resp);
			break;
			
			
		default:
			System.out.println("In default case");
			req.getRequestDispatcher("SEND A 404 HERE").forward(req, resp);

		}

	}

}
