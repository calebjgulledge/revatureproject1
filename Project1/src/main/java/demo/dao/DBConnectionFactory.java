package demo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionFactory {
	
	
	static { //(this would normally go into your dao layer)
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
  }
	
	
	
	private static String dataBaseName = "UndeadDB";
	private static String endPoint = System.getenv("TRAINING_ENDPOINT_DB");
	private static String port = System.getenv("TRAINING_PORT_DB");
	private static String url = "jdbc:postgresql://" + endPoint + ":" + port + "/" + dataBaseName;	
	
	private static String username = System.getenv("TRAINING_USERNAME_DB");
	private static String password = System.getenv("TRAINING_PASSWORD_DB");
	
	
	public static String getUrl() {
		return url;
	}


	public static void setUrl(String url) {
		DBConnectionFactory.url = url;
	}


	public static String getUsername() {
		return username;
	}


	public static void setUsername(String username) {
		DBConnectionFactory.username = username;
	}


	public static String getPassword() {
		return password;
	}


	public static void setPassword(String password) {
		DBConnectionFactory.password = password;
	}


	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}

}
