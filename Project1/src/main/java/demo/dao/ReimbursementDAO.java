package demo.dao;


import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

import demo.model.ReimbursementModel;
import demo.model.UserModel;

public interface ReimbursementDAO {
	
//	Create
	//Employee
	//FinanceManager
	public int insertReimbTicket(UserModel uModel, ReimbursementModel rModel);
	
//	Read
	//Employee
	public ArrayList<ReimbursementModel> selectAllReimbTickets(UserModel uModel, String orderString);
	public ReimbursementModel selectReimbTicketByID(int reimbID, UserModel uModel);
	
	
	//FinanceManager
	public ArrayList<ReimbursementModel> selectAllPendingTickets(String orderString); //Not owned tickets
	public ArrayList<ReimbursementModel> selectAllTickets(UserModel uModel, String orderString);
	public ArrayList<ReimbursementModel> selectAllPendingTicketsByEmployee(String eName);
	public ArrayList<ReimbursementModel> selectAllPendingTicketsBySubmissionDate(Date date);
	
//	Update
	//Employee
	public boolean updateReimbTicketAmount(int reimbID, int newAmount);
	
	//FinanceManager
	public boolean updateReimbStatus(UserModel uModel, int reimbID, int newStatus); //Cannot update own tickets, also needs a service layer that ensures it is a valid int in the DB
	public boolean updateReimbTicketDescription(int reimbID, String newDescr);
	
//	Delete
	//Employee
	//FinanceManager
	public boolean removeReimbTicket(int reimbID); //Own only
}
