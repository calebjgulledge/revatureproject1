package demo.dao;

import demo.model.UserModel;

public interface UserDAO {
	
//	Create
	//No Create functions
	
	
	
//	Read
	public UserModel loginAuthentication(String username, String password);
	public UserModel selectUserbyName(String username);
	public UserModel selectUserbyID(int uModelID);
	
//	Update
	public UserModel updateUsername(UserModel uModel, String newName);
	public UserModel updateEmail(UserModel uModel, String newEmail);
	public UserModel updatePassword(UserModel uModel, String newPassword);
	
	
//	Delete
	//No Delete functions
	

}
