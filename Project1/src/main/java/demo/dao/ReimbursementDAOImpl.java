package demo.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import demo.model.ReimbursementModel;
import demo.model.UserModel;

public class ReimbursementDAOImpl implements ReimbursementDAO {

	@Override
	public int insertReimbTicket(UserModel uModel, ReimbursementModel rModel) {
		int reimbID = -1;
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "INSERT INTO ers_reimbursement (reimb_amount, reimb_submitted, reimb_description, reimb_author, reimb_status_fk, reimb_type_fk) "
					+ "values(?, NOW(), ?, ?, 2, ?) RETURNING reimb_id";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setInt(1, rModel.getReimbAmount());
			ps.setString(2, rModel.getReimbDescription());
			ps.setInt(3, uModel.getUserID());
			ps.setInt(4, rModel.getReimbType());

			ResultSet rs = ps.executeQuery();
			while (rs.next())
				reimbID = rs.getInt(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reimbID;
	}

	@Override
	public ArrayList<ReimbursementModel> selectAllReimbTickets(UserModel uModel, String orderString) {
		ArrayList<ReimbursementModel> rArray = new ArrayList<ReimbursementModel>();
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM ers_reimbursement WHERE reimb_author = ?" + " " + orderString;
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setInt(1, uModel.getUserID());

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ReimbursementModel tempReimb = new ReimbursementModel(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
						rs.getTimestamp(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));

				rArray.add(tempReimb);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rArray;
	}

	@Override
	public ArrayList<ReimbursementModel> selectAllTickets(UserModel uModel, String orderString) {
		ArrayList<ReimbursementModel> rArray = new ArrayList<ReimbursementModel>();
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM ers_reimbursement " + orderString;
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ReimbursementModel tempReimb = new ReimbursementModel(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
						rs.getTimestamp(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));

				rArray.add(tempReimb);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rArray;
	}

	@Override
	public ReimbursementModel selectReimbTicketByID(int reimbID, UserModel uModel) {
		ReimbursementModel tempReimb = new ReimbursementModel();
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM ers_reimbursement WHERE reimb_id = ? AND reimb_author = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setInt(1, reimbID);
			ps.setInt(2, uModel.getUserID());

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				tempReimb = new ReimbursementModel(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempReimb;
	}

	@Override
	public ArrayList<ReimbursementModel> selectAllPendingTickets(String orderString) {
		ArrayList<ReimbursementModel> rArray = new ArrayList<ReimbursementModel>();
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM ers_reimbursement WHERE reimb_status_fk = 2 " + orderString;
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ReimbursementModel tempReimb = new ReimbursementModel(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
						rs.getTimestamp(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));

				rArray.add(tempReimb);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rArray;
	}

	@Override
	public ArrayList<ReimbursementModel> selectAllPendingTicketsByEmployee(String eName) {
		ArrayList<ReimbursementModel> rArray = new ArrayList<ReimbursementModel>();
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM ers_reimbursement er INNER JOIN users u ON er.reimb_author = u.ers_users_id WHERE u.ers_username = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setString(1, eName);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ReimbursementModel tempReimb = new ReimbursementModel(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
						rs.getTimestamp(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));

				rArray.add(tempReimb);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rArray;
	}

	@Override
	public ArrayList<ReimbursementModel> selectAllPendingTicketsBySubmissionDate(Date date) {
		ArrayList<ReimbursementModel> rArray = new ArrayList<ReimbursementModel>();
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM ers_reimbursement WHERE CAST (reimb_submitted AS date) = ?";

			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setDate(1, date);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ReimbursementModel tempReimb = new ReimbursementModel(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3),
						rs.getTimestamp(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));

				rArray.add(tempReimb);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rArray;
	}

	@Override
	public boolean updateReimbTicketAmount(int reimbID, int newAmount) {
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "UPDATE ers_reimbursement SET reimb_amount = ? WHERE reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ps.setInt(1, newAmount);
			ps.setInt(2, reimbID);

			int numOfRowsEffected = ps.executeUpdate();

			if (numOfRowsEffected > 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateReimbStatus(UserModel uModel, int reimbID, int newStatus) {
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "UPDATE ers_reimbursement SET reimb_status_fk = ?, "
					+ "reimb_resolved = NOW(), reimb_resolver = ? WHERE reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ps.setInt(1, newStatus);
			ps.setInt(2, uModel.getUserID());
			ps.setInt(3, reimbID);

			int numOfRowsEffected = ps.executeUpdate();

			if (numOfRowsEffected > 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateReimbTicketDescription(int reimbID, String newDescr) {
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "UPDATE ers_reimbursement SET reimb_description = ? WHERE reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ps.setString(1, newDescr);
			ps.setInt(2, reimbID);

			int numOfRowsEffected = ps.executeUpdate();

			if (numOfRowsEffected > 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean removeReimbTicket(int reimbID) {
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "DELETE FROM ers_reimbursement WHERE reimb_id = ? AND reimb_status_fk = 2";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ps.setInt(1, reimbID);

			int numOfRowsEffected = ps.executeUpdate();

			if (numOfRowsEffected > 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
