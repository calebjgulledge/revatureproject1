package demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.print.attribute.standard.NumberOfDocuments;

import demo.model.UserModel;

public class UserDAOImpl implements UserDAO {

	@Override
	public UserModel loginAuthentication(String username, String password) {
		UserModel tempUser = null;

		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM users WHERE ers_username = ? AND ers_password = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setString(1, username);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				tempUser = new UserModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getInt(7));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempUser;
	}

	@Override
	public UserModel selectUserbyName(String username) {
		UserModel tempUser = null;

		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM users WHERE ers_username = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setString(1, username);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				tempUser = new UserModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getInt(7));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempUser;
	}
	
	@Override
	public UserModel selectUserbyID(int uModelID) {
		UserModel tempUser = null;

		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM users WHERE ers_users_id = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setInt(1, uModelID);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				tempUser = new UserModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getInt(7));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempUser;
	}

	@Override
	public UserModel updateUsername(UserModel uModel, String newName) {
		UserModel tempUser = null;

		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "UPDATE users SET ers_username = ? WHERE ers_users_id = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			
			ps.setString(1, newName);
			ps.setInt(2, uModel.getUserID());

			int numOfRowsEff = ps.executeUpdate();

			if (numOfRowsEff > 0)
				tempUser = selectUserbyID(uModel.getUserID());

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempUser;
	}

	@Override
	public UserModel updateEmail(UserModel uModel, String newEmail) {
		UserModel tempUser = null;

		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "UPDATE users SET user_email = ? WHERE ers_users_id = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ps.setString(1, newEmail);
			ps.setInt(2, uModel.getUserID());
			
			
			int numOfRowsEff = ps.executeUpdate();
			if (numOfRowsEff > 0) {
				tempUser = selectUserbyID(uModel.getUserID());
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempUser;
	}

	@Override
	public UserModel updatePassword(UserModel uModel, String newPassword) {
		UserModel tempUser = null;

		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "UPDATE users SET ers_password = ? WHERE ers_users_id = ?";
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ps.setString(1, newPassword);
			ps.setInt(2, uModel.getUserID());
			
			int numOfRowsEff = ps.executeUpdate();

			if (numOfRowsEff > 0)
				tempUser = selectUserbyID(uModel.getUserID());

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempUser;
	}

}
