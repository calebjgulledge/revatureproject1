package demo.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import demo.model.UserModel;
import demo.service.UserServiceImpl;

public class UserController {
	UserServiceImpl uSer = new UserServiceImpl();

//	Update
	public void updateUsername(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		UserModel uModel = (UserModel) req.getAttribute("loggedInUser");
		boolean success = false;

//		Retrieve Params
		String newName = req.getParameter("newName");

//		Service call
		UserModel newUserModel = uSer.updateUsername(uModel, newName);
		if (newUserModel == null) {
			System.out.println("newUserModel has failed");
		} else {
			req.setAttribute("loggedinUser", newUserModel);
			success = true;
		}

//		Parse object into String
		String rJSON = mapper.writeValueAsString(success);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);

	}

	public void updateEmail(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		UserModel uModel = (UserModel) req.getAttribute("loggedInUser");
		boolean success = false;

//		Retrieve Params
		String newEmail = req.getParameter("newEmail");

//		Service call
		UserModel newUserModel = uSer.updateEmail(uModel, newEmail);
		if (newUserModel == null) {
			System.out.println("newUserModel has failed");
		} else {
			req.setAttribute("loggedinUser", newUserModel);
			success = true;
		}

//		Parse object into String
		String rJSON = mapper.writeValueAsString(success);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);

	}

	public void updatePassword(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		UserModel uModel = (UserModel) req.getAttribute("loggedInUser");
		boolean success = false;

//		Retrieve Params
		String newPassword = req.getParameter("newPassword");

//		Service call
		UserModel newUserModel = uSer.updateEmail(uModel, newPassword);
		if (newUserModel == null) {
			System.out.println("newUserModel has failed");
		} else {
			req.setAttribute("loggedinUser", newUserModel);
			success = true;
		}

//		Parse object into String
		String rJSON = mapper.writeValueAsString(success);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);

	}
	
	public void findUserByID (HttpServletRequest req, HttpServletResponse resp) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(req.getParameter("userID"));
		UserModel uModel = uSer.findUserbyID(Integer.parseInt(req.getParameter("userID")));
		System.out.println(uModel);
		String rJSON = mapper.writeValueAsString(uModel);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
		
	}

	public void forwardAccountOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String myPath = null;

		myPath = "resources/html/accountOptions.html";
		req.getRequestDispatcher(myPath).forward(req, resp);
	}
	
	public void forwardLandingPage(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		UserModel uModel = (UserModel) req.getSession(false).getAttribute("loggedInUser");
		String myPath = null;
		
		if(uModel.getUserRole() == 0) {

			myPath = "resources/html/landingPageManager.html";
			req.getRequestDispatcher(myPath).forward(req, resp);
		}else {

			myPath = "resources/html/landingPageEmployee.html";
			req.getRequestDispatcher(myPath).forward(req, resp);
		}
	}

}
