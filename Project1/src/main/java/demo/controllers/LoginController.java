package demo.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import demo.model.UserModel;
import demo.service.UserServiceImpl;

public class LoginController {

	public void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserServiceImpl userS = new UserServiceImpl();

		String myPath = null;

		if (!req.getMethod().equals("POST")) {
			myPath = "resources/html/loginPageFail.html";
			req.getRequestDispatcher(myPath).forward(req, resp);

		}
//		Retrieve params and uModel to compare
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		UserModel uModel = userS.loginAuthentication(username, password);
		System.out.println(uModel);

		if (uModel != null) {
			req.getSession().setAttribute("loggedInUser", uModel);

			if (uModel.getUserRole() == 0) {

				myPath = "resources/html/landingPageManager.html";
				ObjectMapper mapper = new ObjectMapper();

				String rJSON = mapper.writeValueAsString(false);

				resp.setContentType("application/json");
				resp.getWriter().write(myPath);
//				req.getRequestDispatcher(myPath).forward(req, resp);
			} else {

				myPath = "resources/html/landingPageEmployee.html";
				ObjectMapper mapper = new ObjectMapper();

				String rJSON = mapper.writeValueAsString(false);

				resp.setContentType("application/json");
				resp.getWriter().write(myPath);
//				req.getRequestDispatcher(myPath).forward(req, resp);
			}
		} else {

//			ObjectMapper mapper = new ObjectMapper();

			String rJSON = "";

			resp.setContentType("text");
			resp.getWriter().write(rJSON);
		}

	}

	public void logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String myPath = null;

		req.getSession().invalidate();
		myPath = "resources/html/loginPage.html";
		req.getRequestDispatcher(myPath).forward(req, resp);
	}
	
	
	public void getUserAccount(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserModel user = (UserModel) req.getSession(false).getAttribute("loggedInUser");
		System.out.println(user);
		ObjectMapper mapper = new ObjectMapper();

		String rJSON = mapper.writeValueAsString(user);
		
		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
		
	}

}
