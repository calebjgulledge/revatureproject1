package demo.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import demo.model.ReimbursementModel;
import demo.model.UserModel;
import demo.service.ReimbursementServiceImpl;

public class ReimbursementController {
	ReimbursementServiceImpl rSer = null;
	ObjectMapper mapper = null;
	
	
	
	public ReimbursementController() {
		super();
		this.rSer = new ReimbursementServiceImpl();
		this.mapper = new ObjectMapper();
	}
	
	public ReimbursementController(ReimbursementServiceImpl rSer) {
		super();
		this.rSer = rSer;
	}
	
	public ReimbursementController(ReimbursementServiceImpl rSer, ObjectMapper mapper) {
		super();
		this.rSer = rSer;
		this.mapper = mapper;
	}

	public ReimbursementServiceImpl getrSer() {
		return rSer;
	}

	public void setrSer(ReimbursementServiceImpl rSer) {
		this.rSer = rSer;
	}

	//Create
	//Either
	/**
	 * Both roles can use this function
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public void createReimbTicket(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = this.mapper;
		
		ReimbursementModel tempReimb = mapper.readValue(req.getInputStream(), ReimbursementModel.class);

		UserModel uModel = (UserModel) req.getSession(false).getAttribute("loggedInUser");
		
//		Service call
		int reimbID = rSer.createReimbTicket(uModel, tempReimb);
		//Get the new object
		tempReimb = rSer.findReimbTicketByID(reimbID, uModel);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(tempReimb);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
	}
	
//	Read
	/**
	 * This function is for the employee role only
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public void findAllReimbTickets(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		
		UserModel uModel = (UserModel) req.getSession(false).getAttribute("loggedInUser");
		System.out.println(uModel);
		String orderString = (String) req.getSession(false).getAttribute("orderString");
//		Service call
		ArrayList<ReimbursementModel> reimbArray = rSer.findAllReimbTickets(uModel, orderString);
		System.out.println(reimbArray);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(reimbArray);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
		
	}
	/**
	 * This function is for the manager role
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public void findAllTickets(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		
		UserModel uModel = (UserModel) req.getSession(false).getAttribute("loggedInUser");
		System.out.println(uModel);
		String orderString = (String) req.getSession(false).getAttribute("orderString");
//		Service call
		ArrayList<ReimbursementModel> reimbArray = rSer.findAllTickets(uModel, orderString);
		System.out.println("find all: " + reimbArray);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(reimbArray);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
		
	}
	
	//Either
	/**
	 * This is for both roles
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public void findReimbTicketByID(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		UserModel uModel = (UserModel) req.getSession(false).getAttribute("loggedInUser");
		int reimbID = Integer.parseInt(req.getParameter("reimbID"));
		
//		Service call
		ReimbursementModel tempReimb = rSer.findReimbTicketByID(reimbID, uModel);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(tempReimb);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
	}
	
	
	/**
	 * This is for the manager
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	public void findAllPendingTickets(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		//Not owned tickets
		UserModel uModel = (UserModel) req.getSession(false).getAttribute("loggedInUser");
		if(uModel.getUserRole() != 0) {
			resp.setStatus(404);
			String myPath = "Clearance Issue, not Finance Manager";
			req.getRequestDispatcher(myPath).forward(req, resp);
		}

//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		String orderString = (String) req.getSession(false).getAttribute("orderString");
//		Service call
		ArrayList<ReimbursementModel> reimbArray = rSer.findAllPendingTickets(orderString);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(reimbArray);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
		
	}
	
	/**
	 * For the finance manager only
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	public void findAllPendingTicketsByEmployee(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		UserModel uModel = (UserModel) req.getAttribute("loggedInUser");
		if(uModel.getUserRole() != 0) {
			resp.setStatus(404);
			String myPath = "Clearance Issue, not Finance Manager";
			req.getRequestDispatcher(myPath).forward(req, resp);
		}
		
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		
//		Retrieve Params
		String eName = req.getParameter("eName");
		
//		Service call
		ArrayList<ReimbursementModel> reimbArray = rSer.findAllPendingTicketsByEmployee(eName);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(reimbArray);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
		
		
		
	}
	/**
	 * Unused on the front end
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	public void findAllPendingTicketsBySubmissionDate(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		UserModel uModel = (UserModel) req.getAttribute("loggedInUser");
		if(uModel.getUserRole() != 0) {
			resp.setStatus(404);
			String myPath = "Clearance Issue, not Finance Manager";
			req.getRequestDispatcher(myPath).forward(req, resp);
		}
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		
//		Retrieve Params
		String strDate = req.getParameter("strDate");
		Date sqlDate = Date.valueOf(strDate);
		
//		Service call
		ArrayList<ReimbursementModel> reimbArray = rSer.findAllPendingTicketsBySubmissionDate(sqlDate);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(reimbArray);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
		
	}
	
//	Update
	/**
	 * Extra Unused functionality
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public void updateReimbTicketAmount(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		
//		Retrieve Params
		int reimbID = Integer.parseInt(req.getParameter("reimbID"));
		int newAmount = Integer.parseInt(req.getParameter("newAmount"));
		
//		Service call
		boolean success = rSer.updateReimbTicketAmount(reimbID, newAmount);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(success);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
	}
	
	/**
	 * Manager main function
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	public void updateReimbStatus(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException { 
		UserModel uModel = (UserModel) req.getSession(false).getAttribute("loggedInUser");
		if(uModel.getUserRole() != 0) {
			resp.setStatus(404);
			String myPath = "Clearance Issue, not Finance Manager";
			req.getRequestDispatcher(myPath).forward(req, resp);
		}
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		
//		Retrieve Params
//		int reimbID = Integer.parseInt(req.getParameter("reimbID"));
//		int newStatus = reimbStatusSwitchCase(req.getParameter("reimbStatus"));
		ReimbursementModel tempReimb = mapper.readValue(req.getInputStream(), ReimbursementModel.class);
		
		
		
//		Service call
		boolean success = rSer.updateReimbStatus(uModel, tempReimb.getReimbID(), tempReimb.getReimbStatus());
		if(tempReimb.getReimbDescription() != "") {
			updateReimbTicketDescription(req, resp, tempReimb);
		}
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(success);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
		
	}
	
	/**
	 * Manager Function to give feedback to employee
	 * @param req
	 * @param resp
	 * @param tempReimb
	 * @throws IOException
	 */
	public void updateReimbTicketDescription(HttpServletRequest req, HttpServletResponse resp, ReimbursementModel tempReimb) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		
//		Retrieve Params
//		int reimbID = Integer.parseInt(req.getParameter("reimbID"));
//		String newDescr = req.getParameter("newDescr");
		
//		Service call
		boolean success = rSer.updateReimbTicketDescription(tempReimb.getReimbID(), tempReimb.getReimbDescription());
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(success);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
	}
	
	/**
	 * Functionality not used
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public void deleteReimbTicket(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//		Obligatory JSON mapper
		ObjectMapper mapper = new ObjectMapper();
		
//		Retrieve Params
		int reimbID = Integer.parseInt(req.getParameter("reimbID"));
		
//		Service call
		boolean success = rSer.deleteReimbTicket(reimbID);
		
//		Parse object into String
		String rJSON = mapper.writeValueAsString(success);

		resp.setContentType("application/json");
		resp.getWriter().write(rJSON);
	}
	
	public void setOrderString(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		ObjectMapper mapper = new ObjectMapper();
		
		String orderStr = req.getParameter("orderString");
		String orderType = req.getParameter("orderType");
		
		req.getSession(false).setAttribute("orderString", orderStr);
		
		if (orderType.equals("all")) {
			findAllTickets(req, resp);
		} else if (orderType.equals("pend")) {
			findAllPendingTickets(req, resp);
		}else
			findAllReimbTickets(req, resp);
		
	}
	
	
	
	
	public int reimbTypeSwitchCase(String reimbTypeString) {
		switch(reimbTypeString.toLowerCase()) {
		case "0" :
		case "other" :
			return 0;
		case "1" :
		case "food":
			return 1;
		case "2" :
		case "lodging":
			return 2;
		case "3":
		case "travel":
			return 3;
		default :
			return 0;
			
		}
	}
	
	public int reimbStatusSwitchCase(String reimbStatusString) {
		switch(reimbStatusString.toLowerCase()) {
		case "0" :
		case "denied" :
			return 0;
		case "1" :
		case "approved":
			return 1;
		case "2" :
		case "pending":
			return 2;

		default :
			return 2;
			
		}
	}
	

}
