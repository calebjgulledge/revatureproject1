package test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import demo.dao.ReimbursementDAOImpl;
import demo.model.ReimbursementModel;
import demo.model.UserModel;
import demo.service.ReimbursementServiceImpl;

class ReimbursementServiceTest {

	ReimbursementServiceImpl rServ;
	ReimbursementDAOImpl mockRDAO = mock(ReimbursementDAOImpl.class);

	@BeforeEach
	void setUp() throws Exception {
		rServ = new ReimbursementServiceImpl(mockRDAO);
	}

	@Test
	void testCreateReimbTicket() {
		System.out.println("IN INSERT TEST");
//		CREATE INITIAL VARIABLES
		ReimbursementModel rModel = new ReimbursementModel(200, "Description", 1);
		ReimbursementModel rModel2 = new ReimbursementModel(400, "Description2", 2);
//		CREATE EXPECTED VARIABLES
		int testRMID = 1;
		int testRMID2 = 2;
//		DECLARE MOCKITO'S ROLE
		when(mockRDAO.insertReimbTicket(new UserModel(1), rModel)).thenReturn(testRMID);
		when(mockRDAO.insertReimbTicket(new UserModel(2), rModel2)).thenReturn(testRMID2);
//		ACT
		int rMoID = rServ.createReimbTicket(new UserModel(1), rModel);
		int rMoID2 = rServ.createReimbTicket(new UserModel(2), rModel2);
//		ASSERT
		assertEquals(testRMID, rMoID);
		assertEquals(testRMID2, rMoID2);
		verify(mockRDAO, times(1)).insertReimbTicket(new UserModel(1), rModel);
		verify(mockRDAO, times(1)).insertReimbTicket(new UserModel(2), rModel2);
	}

	@Test
	void testUpdateReimbStatus() {
		System.out.println("IN THE UPDATE REIMB STATUS");
//		CREATE INITIAL VARIABLES
//		CREATE EXPECTED VARIABLES
		boolean expectedOutcome = true;
//		DECLARE MOCKITO'S ROLE
		when(mockRDAO.updateReimbStatus(new UserModel(1), 1, 1)).thenReturn(true);
//		INVOKE METHOD
		boolean actualOutcome = rServ.updateReimbStatus(new UserModel(1), 1, 1);
//		TEST
		assertEquals(expectedOutcome, actualOutcome);

	}

	@Test
	void testSelectAllPendingTickets() {
		System.out.println("IN ALL PENDING TEST");

		ArrayList<ReimbursementModel> initialRModelList = new ArrayList<ReimbursementModel>();
		initialRModelList.add(new ReimbursementModel(2, 400, null, null, "Description2", 2, 0, 2, 2));

		ArrayList<ReimbursementModel> expectedRModelList = new ArrayList<ReimbursementModel>();
		expectedRModelList.addAll(initialRModelList);

		when(mockRDAO.selectAllPendingTickets("")).thenReturn(initialRModelList);

		ArrayList<ReimbursementModel> actualRModelList = rServ.findAllPendingTickets("");
		System.out.println("Expected List: " + expectedRModelList);
		System.out.println("Actual List: " + actualRModelList);

		assertEquals(expectedRModelList, actualRModelList);
		verify(mockRDAO, times(1)).selectAllPendingTickets("");

	}

	@Test
	void testFindAllTickets() {
		System.out.println("IN THE SELECT ALL TICKETS");
		ArrayList<ReimbursementModel> initialRModelList = new ArrayList<ReimbursementModel>();
		initialRModelList.add(new ReimbursementModel(1, 200, null, null, "Description", 1, 1, 1, 1));
		initialRModelList.add(new ReimbursementModel(2, 400, null, null, "Description2", 2, 0, 2, 2));

		ArrayList<ReimbursementModel> expectedRModelList = new ArrayList<ReimbursementModel>();
		expectedRModelList.addAll(initialRModelList);

		when(mockRDAO.selectAllTickets(new UserModel(1), "")).thenReturn(initialRModelList);

		ArrayList<ReimbursementModel> actualRModelList = rServ.findAllTickets(new UserModel(1), "");
		System.out.println("Expected List: " + expectedRModelList);
		System.out.println("Actual List: " + actualRModelList);

		assertEquals(expectedRModelList, actualRModelList);
		verify(mockRDAO, times(1)).selectAllTickets(new UserModel(1), "");
	}

}
