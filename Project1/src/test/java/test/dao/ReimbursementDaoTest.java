package test.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.ClassOrderer.OrderAnnotation;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import demo.dao.DBConnectionFactory;
import demo.dao.ReimbursementDAOImpl;
import demo.model.ReimbursementModel;
import demo.model.UserModel;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ReimbursementDaoTest {

	ReimbursementDAOImpl rDao;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		String dataBaseName = "Pro1Test";
		String endPoint = System.getenv("TRAINING_ENDPOINT_DB");
		String port = System.getenv("TRAINING_PORT_DB");
		String testUrl = "jdbc:postgresql://" + endPoint + ":" + port + "/" + dataBaseName;	
		
		String testUsername = System.getenv("TRAINING_USERNAME_DB");
		String testPassword = System.getenv("TRAINING_PASSWORD_DB");
		DBConnectionFactory.setPassword(testPassword);
		DBConnectionFactory.setUrl(testUrl);
		DBConnectionFactory.setUsername(testUsername);
		dataBaseInit();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		dataBaseDrop();
	}

	@BeforeEach
	void setUp() throws Exception {
		rDao = new ReimbursementDAOImpl();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	@Order(1)
	void testInsertReimbTicket() {
		System.out.println("IN INSERT TEST");
		ReimbursementModel rModel = new ReimbursementModel(200, "Description", 1);
		ReimbursementModel rModel2 = new ReimbursementModel(400, "Description2", 2);
//		System.out.println(rModel);
		int rMoID = rDao.insertReimbTicket(new UserModel(1), rModel);
		int rMoID2 = rDao.insertReimbTicket(new UserModel(2), rModel2);

		assertEquals(1, rMoID);
		assertEquals(2, rMoID2);
	}
	@Test
	@Order(2)
	void testUpdateReimbStatus() {
		System.out.println("IN THE UPDATE REIMB STATUS");
		boolean expectedOutcome = true;
		
		boolean actualOutcome = rDao.updateReimbStatus(new UserModel(1), 1, 1);
		
		assertEquals(expectedOutcome, actualOutcome);
	}

	@Test
	@Order(3)
	void testSelectAllPendingTickets() {
		System.out.println("IN ALL PENDING TEST");
		ArrayList<ReimbursementModel> initialRModelList = new ArrayList<ReimbursementModel>();
		initialRModelList.add(new ReimbursementModel(2, 400, null, null, "Description2", 2, 0, 2, 2));

		ArrayList<ReimbursementModel> expectedRModelList = new ArrayList<ReimbursementModel>();
		expectedRModelList.addAll(initialRModelList);

		ArrayList<ReimbursementModel> actualRModelList = rDao.selectAllPendingTickets("");
		System.out.println("Expected List: " + expectedRModelList);
		System.out.println("Actual List: " + actualRModelList);

		assertEquals(expectedRModelList, actualRModelList);
	}
	@Test
	@Order(4)
	void testSelectAllTickets() {
		System.out.println("IN THE SELECT ALL TICKETS");
		ArrayList<ReimbursementModel> initialRModelList = new ArrayList<ReimbursementModel>();
		initialRModelList.add(new ReimbursementModel(1, 200, null, null, "Description", 1, 1, 1, 1));
		initialRModelList.add(new ReimbursementModel(2, 400, null, null, "Description2", 2, 0, 2, 2));

		ArrayList<ReimbursementModel> expectedRModelList = new ArrayList<ReimbursementModel>();
		expectedRModelList.addAll(initialRModelList);

		ArrayList<ReimbursementModel> actualRModelList = rDao.selectAllTickets(new UserModel(1), "ORDER BY reimb_id ASC");
		System.out.println("Expected List: " + expectedRModelList);
		System.out.println("Actual List: " + actualRModelList);

		assertEquals(expectedRModelList, actualRModelList);
	}
	
	
	
	
	private static void dataBaseInit() {
		System.out.println("IN THE DATABASE INIT");
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "CREATE TABLE reimbursement_status( reimb_status_id INT PRIMARY KEY "
					+ "	, reimb_status VARCHAR(10) NOT NULL ); CREATE TABLE reimbursement_type( "
					+ "	reimb_type_id INT PRIMARY KEY  	, reimb_type VARCHAR(10) NOT NULL  ); "
					+ "CREATE TABLE user_roles(user_role_id INT PRIMARY KEY "
					+ "	, user_role VARCHAR(20) NOT NULL ); CREATE TABLE users( "
					+ "	ers_users_id SERIAL PRIMARY KEY 	, ers_username VARCHAR(50) UNIQUE NOT NULL "
					+ "	, ers_password VARCHAR(50) NOT NULL 	, user_first_name VARCHAR(100) NOT NULL "
					+ "	, user_last_name VARCHAR(100) NOT NULL 	, user_email VARCHAR(150) UNIQUE NOT NULL "
					+ "	, user_role_fk INT NOT NULL "
					+ "	, FOREIGN KEY (user_role_fk) REFERENCES user_roles (user_role_id) ); "
					+ "CREATE TABLE ers_reimbursement( 	reimb_id SERIAL PRIMARY KEY "
					+ "	, reimb_amount INT NOT NULL 	, reimb_submitted TIMESTAMP NOT NULL "
					+ "	, reimb_resolved TIMESTAMP 	, reimb_description VARCHAR(250) "
					+ "	, reimb_author INT NOT NULL 	, reimb_resolver INT 	, reimb_status_fk INT NOT NULL "
					+ "	, reimb_type_fk INT NOT NULL "
					+ "	, FOREIGN KEY (reimb_author) REFERENCES users (ers_users_id) "
					+ "	, FOREIGN KEY (reimb_resolver) REFERENCES users (ers_users_id) "
					+ "	, FOREIGN KEY (reimb_status_fk) REFERENCES reimbursement_status (reimb_status_id) "
					+ "	, FOREIGN KEY (reimb_type_fk) REFERENCES reimbursement_type (reimb_type_id) ); "
					+ "INSERT INTO reimbursement_status (reimb_status_id, reimb_status) 	VALUES (0, 'DENIED'); "
					+ "INSERT INTO reimbursement_status (reimb_status_id, reimb_status) "
					+ "	VALUES (1, 'APPROVED'); INSERT INTO reimbursement_status (reimb_status_id, reimb_status) "
					+ "	VALUES (2, 'PENDING'); INSERT INTO reimbursement_type (reimb_type_id, reimb_type) "
					+ "	VALUES (0, 'OTHER'); INSERT INTO reimbursement_type (reimb_type_id, reimb_type) "
					+ "	VALUES (1, 'FOOD'); INSERT INTO reimbursement_type (reimb_type_id, reimb_type) "
					+ "	VALUES (2, 'LODGING'); INSERT INTO reimbursement_type (reimb_type_id, reimb_type) "
					+ "	VALUES (3, 'TRAVEL'); INSERT INTO user_roles (user_role_id, user_role) "
					+ "	VALUES (0, 'Finance Manager'); INSERT INTO user_roles (user_role_id, user_role) "
					+ "	VALUES (1, 'Employee'); "
					+ "INSERT INTO users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_fk) "
					+ "VALUES ('jack.skellington', 'password0', 'Jack', 'Skellington', 'whatsthiswhatsthat@gmail.com', 0); "
					+ "INSERT INTO users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_fk) "
					+ "VALUES ('elijah.deadman', 'password1', 'Elijah', 'Deadman', 'eli.dead@gmail.com', 1); "
					+ "INSERT INTO users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_fk) "
					+ "VALUES ('dry.bones', 'password2', 'Dry', 'Bones', 'mr.bones@gmail.com', 1); "
					+ "INSERT INTO users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_fk) "
					+ "VALUES ('abby.cadaver', 'password3', 'Abby', 'Cadaver', 'habiocorpio@gmail.com', 1)";

			Statement state = conn.createStatement();
			state.execute(ourSQLStatement);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static void dataBaseDrop() {
		System.out.println("IN THE DATABASE DROP");
		try (Connection conn = DBConnectionFactory.getConnection()) {
			String ourSQLStatement = "DROP TABLE ers_reimbursement; " + "DROP TABLE users; " + "DROP TABLE user_roles; "
					+ "DROP TABLE reimbursement_status; " + "DROP TABLE reimbursement_type";

			Statement state = conn.createStatement();
			state.execute(ourSQLStatement);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
