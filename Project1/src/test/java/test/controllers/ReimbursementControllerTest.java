package test.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import demo.controllers.ReimbursementController;
import demo.model.ReimbursementModel;
import demo.model.UserModel;
import demo.service.ReimbursementServiceImpl;
import test.util.DelegatingServletInputStream;

class ReimbursementControllerTest {
	
	ReimbursementController rCon;

	ObjectMapper mockMapper = mock(ObjectMapper.class);
	HttpServletResponse httpResponseMock = mock(HttpServletResponse.class);
	HttpServletRequest httpRequestMock = mock(HttpServletRequest.class);
	ServletInputStream mockStream = mock(ServletInputStream.class);
	PrintWriter printerMock = mock(PrintWriter.class);
	ReimbursementServiceImpl mockServ = mock(ReimbursementServiceImpl.class);
	HttpSession mockSess = mock(HttpSession.class);
	JsonParser mockParse = mock(JsonParser.class);

	@BeforeEach
	void setUp() throws Exception {
		rCon = new ReimbursementController(mockServ, mockMapper);
	}


	@Test
	void testCreateReimbTicket() throws StreamReadException, DatabindException, IOException {
		System.out.println("IN INSERT TEST");
//		ARRANGE
		UserModel uModel = new UserModel(1);
		mockSess.setAttribute("loggedInUser", uModel);
		ReimbursementModel tempReimb = new ReimbursementModel(200, "Description", 1);
		int reimbID = uModel.getUserID();
		
		
		String testReimbStr = new ObjectMapper().writeValueAsString(new ReimbursementModel());
		ByteArrayInputStream anyInputStream = new ByteArrayInputStream(testReimbStr.getBytes());
		DelegatingServletInputStream DSIS = new DelegatingServletInputStream(anyInputStream);
		


//		verify(mockMapper, times(1)).readValue(any(ServletInputStream.class), eq(RequestObject.class));
		
		
		
//		SPRING TEST SOLVES THIS PROBLEM INSTANTLY
		when(httpRequestMock.getInputStream()).thenReturn(DSIS);
		when(mockMapper.readValue(httpRequestMock.getInputStream(), ReimbursementModel.class)).thenReturn(tempReimb);

		
		when(httpRequestMock.getSession(false)).thenReturn(mockSess);
		
		when(mockSess.getAttribute("loggedInUser")).thenReturn((UserModel)uModel);
		when(mockServ.createReimbTicket(uModel, tempReimb)).thenReturn(reimbID);
		when(mockServ.findReimbTicketByID(reimbID, uModel)).thenReturn(tempReimb);
		when(httpResponseMock.getWriter()).thenReturn(printerMock);
			
//		ACT
		rCon.createReimbTicket(httpRequestMock, httpResponseMock);
		
		
//		ASSERT
		verify(mockMapper, times(1)).readValue(httpRequestMock.getInputStream(), ReimbursementModel.class);
		
		verify(httpRequestMock, times(1)).getSession(false);
		verify(mockSess, times(1)).getAttribute("loggedInUser");
		
		verify(mockServ, times(1)).createReimbTicket(uModel, tempReimb);
		verify(mockServ, times(1)).findReimbTicketByID(reimbID, uModel);
		verify(printerMock, times(1)).write(testReimbStr);
		verify(httpResponseMock, times(1)).getWriter();
		
		
		
	}

	@Test
	void testUpdateReimbStatus() {

	}

	@Test
	void testFindAllPendingTickets() {
		
	}

	@Test
	void testFindAllTickets() {
		
	}

}
