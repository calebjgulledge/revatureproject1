# Expense Reimbursement System (ERS)

## Project Description

The Expense Reimbursement System (ERS) will manage the process of reimbursing
employees for expenses incurred while on company time. All employees in the
company can login and submit requests for reimbursement and view their past tickets
and pending requests. Finance managers can log in and view all reimbursement
requests and past history for all employees in the company. Finance managers are
authorized to approve and deny requests for expense reimbursement.


## Technologies Used

* Java Servlets - version 4.0.1
* PostgreSQL - version 42.3.3
* ProjectLombok - version 1.18.22
* JUnit - version 5.8.2
* Mockito - version 4.2.0
* H2DataBase - version 2.1.212

## Features

List of features ready and TODOs for future development
* Login Feature
* Employees can view past tickets and create new ones.
* Finance Managers can view all tickets and approve/deny them. (They can also filter requests by status)

To-do list:
* Have Employee's cancel the request or update it before it gets approved/denied
* Have Finance Manager's put in tickets, but be unable to approve their own.

## Getting Started
>git clone command
    git clone https://gitlab.com/calebjgulledge/revatureproject1.git


>The DAO layer was written for PostgreSQL databases and if interchanged the prepared statements will need to be changed to reflect that change.

## Usage

> If you decide to use it, then I am glad it could be of service to you.
>To properly use this project, you will need to set up system ENV variables that your IDE will read on start-up. They need to be mapped to the user name and password connected to the data base. You will also need to have an endpoint:port that directs you to the database. 

## License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
